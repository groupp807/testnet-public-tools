# Q Onboarding in 5 Minutes

## Install MetaMask

First, install the browser add-on 'MetaMask'. After successful installation, add a custom network and enter the Q network configuration details that are documented within [MetaMask tutorial](how-to-install-metamask.md). Finally, create two new accounts so you can later send Q tokens from one to another.

## Network Configuration

You might need to configure your wallet to support and accept Q tokens. To do this, you need to connect to the Q testnet by using the following network details:

| **Parameter** | **Value** |
|:--|:--|
| Network name | Q Testnet|
| RPC-URL | https://rpc.qtestnet.org |
| Chain-ID | 35443 |
| Symbol | Q |
| Block-Explorer-URL| https://explorer.qtestnet.org |

> **Note: ** *These parameters are valid for Q testnet only!*

## Get your first Q

If you don't own Q tokens already, get some for free using our faucet at [`https://faucet.qtestnet.org`](https://faucet.qtestnet.org). Enter your wallet address, select the token you want to receive (Q, QUSD, QBTC, QUSDC, QDAI), solve Captcha, and click on "Claim tokens".

Here are some rules for claiming tokens via faucet:

- You can fund the same account address only once in a day
- You can claim tokens up to 10 times per device in a day
- You can't claim tokens if you have equal (or more) amount of appropriate tokens on your account balance


If the faucet is funded enough, your balance will increase within a couple of seconds. If the faucet has run out of funds or displays unknown error, drop us a note on our [Discord](https://discord.gg/YTgkvJvZGD) server.

## Verify Q Funding

After some seconds, open MetaMask and select the wallet account you just funded. It should now have received a couple of Qs and should display an according balance.

## Send Q

Within MetaMask, click on "send", then "transfer between my accounts" and select the second wallet account you just created. Finish the transaction and wait for the entered amount of Q to be transferred from account to account. You can verify the transaction within the Q block explorer located at [`https://explorer.qtestnet.org`](https://explorer.qtestnet.org). You can of course send Q to any other wallet address you know.

## Earn Rewards

Now open a new tab and head over to [`https://hq.qtestnet.org`](https://hq.qtestnet.org). If you have MetaMask still activated and connected to Q testnet, you should be browsing the Q decentralized app that allows users to [exercise governance rights](how-to-exercise-governance-rights.md), [earn rewards on Q tokens](how-to-earn-extra-Q-tokens.md) or do some [saving and borrowing with Q DeFi](how-to-obtain-a-loan-against-a-collateral.md).
